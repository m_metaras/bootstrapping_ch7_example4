# Sets the "backend" used to store Terraform state.
# This is required to make continous delivery work.

terraform {
  backend "azurerm" {
    resource_group_name  = "flixtube-terraform6706"
    storage_account_name = "flixtubeterraform6706"
    container_name       = "terraform-state6706"
    key                  = "terraform.tfstate"
  }
}